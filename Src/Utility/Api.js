
export const URL = 'http://65.108.74.195:3001'

const baseUrl = 'http://65.108.74.195:3001/api/v1/'
import axios from 'axios'

export function getServices(data) {

    // let headers ={
    //     client : keys?.client,
    //     uid: keys?.uid,
    //     access_token: keys?.accessToken
    //   }

    return new Promise((resolve, reject) => {
        axios.get(`${baseUrl}service/fetch_services`, {
            // headers: headers,
            params: {
                tags: data,
                locale: 'en'
            },
        }).then((res) => {
            resolve(res)
        })
    }).catch((err) => {
        reject(err)
    })
}


export function getServicesById(id) {
    return new Promise((resolve, reject) => {
        axios.get(`${baseUrl}service/show_service`, {
            params: {
                id: id,
                locale: 'en'
            },
        }).then((res) => {
            resolve(res)
        })
    }).catch((err) => {
        reject(err)
    })
}

export function getTimeSlots(obj) {
    return new Promise((resolve, reject) => {
        axios.get(`${baseUrl}service/time_slots`, {
            params: {
                service_provider_id: obj.id,
                day: obj.day,
                locale: 'en'
            },
        }).then((res) => {
            resolve(res)
        })
    }).catch((err) => {
        reject(err)
    })
}

export function bookAppointment(obj) {
    return new Promise((resolve, reject) => {
        axios.post(`${baseUrl}service/book_appointment`, {
            params: {
                service_id: obj.id,
                time_slot: obj.t_slot,
                day_slot: obj.d_slot
            }
        }).then((res) => {
            resolve(res)
        }).catch((err) => {
            reject(err)
        })
    })
}