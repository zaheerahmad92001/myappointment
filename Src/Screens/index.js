import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import Splash from '../View/Splash';
import Login from '../View/Login';
import Home from '../View/Home';
import Listing from '../View/Listing';
import BookAppointment from '../View/BookAppointment';
import AppointmentFixed from '../View/AppointmentFixed';

const Stack = createNativeStackNavigator();
const Tab = createBottomTabNavigator();

function SplashStack(){
    return (
        <Stack.Navigator initialRouteName={"Splash"} screenOptions={{ headerShown: false }}>
            <Stack.Screen name={'Splash'} component={Splash} />
        </Stack.Navigator>
    )
  }

  function AuthStack(){
    return (
        <Stack.Navigator initialRouteName={"Login"} screenOptions={{ headerShown: false }}>
            <Stack.Screen name={'Login'} component={Login} />
        </Stack.Navigator>
    )
  }

  function HomeStack(){
    return (
        <Stack.Navigator initialRouteName={"Home"} screenOptions={{ headerShown: false }}>
            <Stack.Screen name={'Home'} component={Home} />
            <Stack.Screen name={'Listing'} component={Listing} />
            <Stack.Screen name={'BookAppointment'} component={BookAppointment} />
            <Stack.Screen name={'AppointmentFixed'} component={AppointmentFixed} />
        </Stack.Navigator>
    )
  }


  function AppContainer() {
    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="SplashSreen" screenOptions={{ headerShown: false }}>

          <Stack.Screen name={'SplashSreen'} component={SplashStack} />
          <Stack.Screen name={'AuthStack'} component={AuthStack} />
          <Stack.Screen name={'HomeStack'} component={HomeStack} />

        </Stack.Navigator>
      </NavigationContainer>
    )
  }

  export default AppContainer