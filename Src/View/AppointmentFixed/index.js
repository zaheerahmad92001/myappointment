import React, { useEffect, useReducer, useRef } from "react";
import { View, FlatList, Image, Text, TouchableHighlight, TouchableOpacity } from "react-native";

import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import { docImg, } from '../../Constants/Images'
import ModalDropdown from 'react-native-modal-dropdown';
import ItemCart from "../../Components/ItemCard";
import { Icon } from "react-native-elements/dist/icons/Icon";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { green, darkGreen } from "../../Constants/Colors";
import QRCode from 'react-native-qrcode-svg';
import { Divider } from "react-native-elements";

const DEMO_OPTIONS_1 = [
    { "name": "Rex", "age": 30 },
    { "name": "Mary", "age": 25 },
    { "name": "John", "age": 41 },
    { "name": "Jim", "age": 22 },
    { "name": "Susan", "age": 52 },
    { "name": "Brent", "age": 33 },
    { "name": "Alex", "age": 16 },
    { "name": "Ian", "age": 20 },
    { "name": "Phil", "age": 24 },
];

const DEMO_OPTIONS_2 = [
    { "name": "RexDoctore", "age": 30 },
    { "name": "MaryDoctore", "age": 25 },
    { "name": "JohnDoctore", "age": 41 },
    { "name": "JimDoctore", "age": 22 },
    { "name": "SusanDoctore", "age": 52 },
    { "name": "BrentDoctore", "age": 33 },
    { "name": "AlexDoctore", "age": 16 },
    { "name": "IanDoctore", "age": 20 },
    { "name": "PhilDoctore", "age": 24 },
];

function AppointmentFixed({ navigation, route }) {

    clinicRef = useRef()
    docRef = useRef()
    useEffect(() => {

    }, [])

    const [state, updateState] = useReducer((
        (state, newState) => ({ ...state, ...newState })
    ), {
        clinic: '',
        doc: '',
    })
    const { clinic, doc } = state
    let time = route.params.params.item
    let serviceList = route.params.params.serviceList

    return (
        <View style={styles.wraper}>
            <AppHeader
                // title={'Book Appointment'}
                leftIconName={'arrow-back-ios'}
                leftIconType={'material'}
                leftPress={() => { navigation.pop() }}
            />
            <View style={styles.container}>
                <View style={styles.doneView}>
                    <Text style={styles.bigText}>Done</Text>
                    <Icon
                        name="check"
                        type="font-awesome-5"
                        iconStyle={{ fontSize: 30, marginLeft: 10, color: darkGreen }}
                    />
                </View>

                <View style={styles.qrcode}>
                    <QRCode
                        value="http://awesome.link.qr"
                        size={200}
                    />
                </View>
                <Text style={styles.mediumText}>{'Please present this qr code at your appointment'}</Text>

                <View style={[styles.doneView, { marginTop: hp(4) }]}>
                    <Text style={styles.standeredText}>{'Alhabeeb Hospital'}</Text>
                    <View style={[styles.circle,]}><Icon type={'ionicon'} name={'chevron-forward'} iconStyle={{ color: green, fontSize: 20 }} /></View>
                </View>

                <Divider
                    style={{ alignSelf: "center", width: wp(60), marginTop: hp(1) }}
                />
                <View style={styles.drNameView}>
                    <Text style={styles.standeredText}>{`${serviceList.title}`}</Text>
                </View>
                <View style={styles.drNameView}>
                    <Text style={[styles.standeredText, { color: darkGreen }]}>{`${time?.t_slot}`}</Text>
                </View>

                {/* <View style={[styles.doneView, { marginTop: hp(3) }]}>
                    <TouchableOpacity
                    onPress={()=>alert('this feature comming soon')}
                    >
                        <Icon type="ionicon" name="location-sharp"
                            iconStyle={{ fontSize: 40, color: darkGreen }}
                        />
                        <Text style={{ ...styles.standeredText, fontSize: 12 }}>{'location'}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                    onPress={()=>alert('this feature comming soon')}
                     style={{ marginLeft: wp(15) }}>
                        <Icon type="material" name="edit"
                            iconStyle={{ fontSize: 40, color: darkGreen }}
                        />
                        <Text style={{ ...styles.standeredText, fontSize: 12 }}>{'edit'}</Text>
                    </TouchableOpacity>
                </View> */}
            </View>
        </View>
    )

}
export default AppointmentFixed
