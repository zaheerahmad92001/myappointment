import { StyleSheet } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appMarginHorizontal, mediumSize, small, } from "../../Constants/appFontSize";
import { black, darkGreen, green, lightGreen, white } from "../../Constants/Colors";

const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginTop: hp(3),
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },
    doneView: {
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: "center",
    },
    bigText:{
      color:darkGreen,
      fontSize:RFValue(30),
      fontWeight:'600',
    },
    qrcode:{
        // flex:1,
        // justifyContent:"center",
        alignSelf:'center',
        marginTop:hp(5)

    },
    mediumText:{
        fontSize:mediumSize,
        fontWeight:'500',
        color:green,
        textAlign:'center',
        marginTop:hp(2)
        
    },
    circle: {
        width: 25,
        height: 25,
        borderRadius: 25 / 2,
        backgroundColor: lightGreen,
        marginLeft:wp(10),
        alignItems: 'center',
    },
    standeredText:{
        fontSize:small,
        fontWeight:'500',
        color:black,
    },
    drNameView:{
        marginTop:hp(2),
        // justifyContent:'center',
        alignItems:'flex-start',
        backgroundColor:lightGreen,
        paddingVertical:10,
        paddingLeft:10,
        marginHorizontal:wp(10),
    },

})

export default styles