import React, { useEffect, useReducer, useRef } from "react";
import { View, FlatList, Image, Text, TouchableHighlight, TouchableOpacity, ScrollView, ActivityIndicator } from "react-native";

import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import { docImg, } from '../../Constants/Images'
import ModalDropdown from 'react-native-modal-dropdown';
import ItemCart from "../../Components/ItemCard";
import { Icon } from "react-native-elements/dist/icons/Icon";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { darkGreen, green, white } from "../../Constants/Colors";
import { getServicesById, getTimeSlots, bookAppointment } from "../../Utility/Api";
import { URL } from "../../Utility/Api";
import moment from "moment";


const DEMO_OPTIONS_1 = [
    { "name": "Rex", "age": 30 },
    { "name": "Mary", "age": 25 },
    { "name": "John", "age": 41 },
    { "name": "Jim", "age": 22 },
    { "name": "Susan", "age": 52 },
    { "name": "Brent", "age": 33 },
    { "name": "Alex", "age": 16 },
    { "name": "Ian", "age": 20 },
    { "name": "Phil", "age": 24 },
];

const DEMO_OPTIONS_2 = [
    { "name": "RexDoctore", "age": 30 },
    { "name": "MaryDoctore", "age": 25 },
    { "name": "JohnDoctore", "age": 41 },
    { "name": "JimDoctore", "age": 22 },
    { "name": "SusanDoctore", "age": 52 },
    { "name": "BrentDoctore", "age": 33 },
    { "name": "AlexDoctore", "age": 16 },
    { "name": "IanDoctore", "age": 20 },
    { "name": "PhilDoctore", "age": 24 },
];

function BookAppointment({ navigation, route }) {

    clinicRef = useRef()
    docRef = useRef()

    useEffect(() => {
        let serviceId = route.params.params.item.id
        fetchServiceById(serviceId)
    }, [])

    const [state, updateState] = useReducer((
        (state, newState) => ({ ...state, ...newState })
    ), {
        clinic: '',
        doc: '',
        loading: false,
        tsLoading: false,
        serviceList: [],
        av_dates: [],
        avatar: undefined,
        timeSlots: '',
        selectedTime: '',
        selectedDay: '',
        c_date: moment().format('MMMM Do YYYY'),
        processing: false,

    })
    const {
        clinic, doc,
        loading,
        serviceList,
        av_dates,
        avatar,
        tsLoading,
        timeSlots,
        selectedTime,
        selectedDay,
        c_date,
        processing
    } = state

    async function fetchServiceById(id) {
        updateState({ loading: true })
        await getServicesById(id).then((res) => {
            const { data } = res
            if (data?.api_status) {
                let tempDates = []
                data?.available_dates.map((item, index) => {
                    let obj = {
                        date: item,
                        selected: false
                    }
                    tempDates.push(obj)
                })

                updateState({
                    loading: false,
                    // av_dates:data?.available_dates,
                    av_dates: tempDates,
                    serviceList: data?.service,
                    avatar: data.service.attachments[0].url,
                })
            }
            // console.log('here is Service detail', data);
        })
    }



    // function handleShopList(tab) {
    //     console.log('seleted tab', tab);
    // }


    // function renderShopItem() {
    //     return (
    //         <ItemCart />
    //     )
    // }

    // function renderRightComponent_1() {
    //     return (
    //         <Icon
    //             name="chevron-down"
    //             type="entypo"
    //             iconStyle={{
    //                 color: 'black',
    //                 marginLeft: wp(55),
    //                 fontSize: 20,
    //             }}
    //         />
    //     )
    // }

    // function renderRightComponent() {
    //     return (
    //         <Icon
    //             name="chevron-down"
    //             type="entypo"
    //             iconStyle={{
    //                 color: 'black',
    //                 marginLeft: wp(55),
    //                 fontSize: 20,
    //             }}
    //         />
    //     )
    // }

    // function _dropdown_1_clinic(rowData, rowID, highlighted) {

    //     return (
    //         <TouchableHighlight
    //             onPress={() => {
    //                 clinicRef.current.select(rowID);
    //                 clinicRef.current.hide()
    //                 updateState({ clinic: rowData })
    //             }}
    //             style={styles.dropdown_row}
    //             underlayColor='cornflowerblue'>
    //             <View>
    //                 <Text style={[styles.dropdown_row_text, highlighted && { color: green }]}>
    //                     {`${rowData.name}`}
    //                 </Text>
    //             </View>
    //         </TouchableHighlight>
    //     );
    // }

    // function _dropdown_2_doc(rowData, rowID, highlighted) {

    //     return (
    //         <TouchableHighlight
    //             onPress={() => {
    //                 docRef.current.select(rowID);
    //                 docRef.current.hide()
    //                 updateState({ doc: rowData })
    //             }}
    //             style={styles.dropdown_row}
    //             underlayColor='cornflowerblue'>
    //             <View>
    //                 <Text style={[styles.dropdown_row_text, highlighted && { color: green }]}>
    //                     {`${rowData.name}`}
    //                 </Text>
    //             </View>
    //         </TouchableHighlight>
    //     );
    // }


    // function _dropdown_2_renderButtonText(rowData) {
    //     const { name, age } = rowData;
    //     return `${name}`;
    // }

    function renderDate({ item, index }) {
        return (
            <TouchableOpacity
                onPress={() => handleDateSelection(item, index)}
                style={item.selected ? [styles.dateViewSelect] : [styles.dateView]}>
                <Text style={item.selected ? [styles.dateTextSelect] : [styles.dateText]}>{item.date?.split(' ')[0]}</Text>
                <Text style={item.selected ? [styles.dateTextSelect] : [styles.dateText]}>{item.date?.split(' ')[1]}</Text>
            </TouchableOpacity>
        )
    }

    const handleDateSelection = async (item, index) => {
        let dateList = av_dates.slice(0)
        let temp = []
        dateList.map((d, i) => {
            if (i == index) {
                dateList[i].selected = true
            } else {
                dateList[i].selected = false
            }
            updateState({ av_dates: dateList, selectedDay: item })
        })


        updateState({ tsLoading: true })
        let p_id = serviceList.service_provider_id
        let day = item.date.split(' ')[0]
        let obj = {
            id: p_id,
            day: day
        }
        await getTimeSlots(obj).then((res) => {
            const { data } = res
            if (data?.api_status) {
                let tempSlot = []
                data?.time_slots.map((time, index) => {
                    let __obj = {
                        selected: false,
                        t_slot: time
                    }
                    tempSlot.push(__obj)
                })
                updateState({
                    tsLoading: false,
                    timeSlots: tempSlot
                })
            }
        })
    }

    function renderTime({ item, index }) {
        return (
            <TouchableOpacity
                onPress={() => handleTime(item, index)}
                style={item.selected ? [styles.timeContainerSelected] : [styles.timeContainer]}>
                <Text style={[styles.tiemText]}>{`${item.t_slot}`}</Text>
            </TouchableOpacity>
        )
    }

    const handleTime = (item, index) => {
        let tempTime = timeSlots.slice()
        tempTime.map((t, i) => {
            if (i == index) {
                tempTime[i].selected = true
            } else {
                tempTime[i].selected = false
            }
        })
        updateState({ selectedTime: item, timeSlots: tempTime })
    }

    const handleSave = async () => {
        if (selectedTime) {
            updateState({ processing: true })
            let serviceId = route.params.params.item.id
            let obj = {
                id: serviceId,
                t_slot: selectedTime.t_slot,
                d_slot: selectedDay.date,
            }
            await bookAppointment(obj).then((res) => {
                const { data } = res
                if (data?.api_status) {
                    updateState({ processing: false })
                    navigation.navigate('AppointmentFixed', {
                        params: {
                            item: selectedTime,
                            serviceList
                        }
                    })
                } else {
                    alert('something went wrong')
                }
                console.log('here is data response', data);
            })

        } else {
            alert('Please select time slot')
        }
    }


    return (

        <View style={styles.wraper}>
            <AppHeader
                title={'Book Appointment'}
                leftIconName={'arrow-back-ios'}
                leftIconType={'material'}
                leftPress={() => { navigation.pop() }}
            />

            <View style={styles.container}>
                {loading ?
                    <ActivityIndicator
                        color={darkGreen}
                        size={'small'}
                    /> :
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                    >
                        <View style={styles.docView}>
                            <View style={styles.imgView}>
                                <Image
                                    source={{ uri: `${URL}${avatar}` }}
                                    style={styles.imageStyle}
                                />
                            </View>
                            <View style={styles.nameView}>
                                <Text style={styles.nameText}>{serviceList?.title}</Text>
                            </View>
                        </View>
                        <Text style={styles.ticketNum}>{'MP# 12454'}</Text>
                        {/* <View style={styles.ddOuterView}>
                            <Text style={styles.ddHeader}>Clinic</Text>
                            <ModalDropdown
                                ref={clinicRef}
                                defaultValue={'Please Select'}
                                options={DEMO_OPTIONS_1}
                                style={styles.modalDropdown}
                                dropdownTextStyle={styles.dropdownTextStyle}
                                dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
                                renderButtonText={(rowData) => _dropdown_2_renderButtonText(rowData)}
                                renderRow={_dropdown_1_clinic}
                                //  dropdownTextProps={{backgroundColor:"red"}}
                                textStyle={styles.textStyle}
                                renderRightComponent={renderRightComponent_1}
                                buttonAndRightComponentContainerStyle={{ justifyContent: 'space-between', marginRight: 10 }}
                                dropdownStyle={styles.dropdownStyle}
                                isFullWidth={true}
                            />
                        </View> */}


                        {/* <View style={styles.ddOuterView}>
                            <Text style={styles.ddHeader}>Doctor</Text>
                            <ModalDropdown
                                ref={docRef}
                                defaultValue={'Please Select'}
                                options={DEMO_OPTIONS_2}
                                style={styles.modalDropdown}
                                dropdownTextStyle={styles.dropdownTextStyle}
                                dropdownTextHighlightStyle={styles.dropdownTextHighlightStyle}
                                renderButtonText={(rowData) => _dropdown_2_renderButtonText(rowData)}
                                renderRow={_dropdown_2_doc}
                                //  dropdownTextProps={{backgroundColor:"red"}}
                                textStyle={styles.textStyle}
                                renderRightComponent={renderRightComponent}
                                buttonAndRightComponentContainerStyle={{ justifyContent: 'space-between', marginRight: 10 }}
                                dropdownStyle={styles.dropdownStyle}
                                isFullWidth={true}
                            />
                        </View> */}

                        <View style={styles.timOuterView}>
                            <Text style={styles.montText}>{`${c_date.split(' ')[0]} ${c_date.split(' ')[2]}`}</Text>
                            <FlatList
                                data={av_dates}
                                keyExtractor={(item) => item.id}
                                renderItem={renderDate}
                                horizontal={true}
                                showsHorizontalScrollIndicator={false}
                            />
                        </View>
                        <View style={styles.timOuterView}>
                            <Text style={styles.timeText}>Time</Text>
                            {tsLoading ?
                                <ActivityIndicator
                                    color={darkGreen}
                                    size={'small'}
                                /> : timeSlots.length > 0 ?
                                    <FlatList
                                        data={timeSlots}
                                        keyExtractor={(item) => item.id}
                                        renderItem={renderTime}
                                        // horizontal={true}
                                        columnWrapperStyle={{ justifyContent: 'space-between' }}
                                        numColumns={3}
                                        scrollEnabled={false}
                                        style={{ marginBottom: 20 }}
                                        showsHorizontalScrollIndicator={false}
                                    />
                                    : <Text>{'Please Select Day to View available slots'}</Text>
                            }
                        </View>
                        <View style={{marginBottom:20,}}>
                        {processing ?
                            <ActivityIndicator
                                color={darkGreen}
                                size={'small'}
                            /> :
                            <TouchableOpacity
                                onPress={() => handleSave()}
                                style={timeSlots.length > 0 ? [styles.buttonView] : [styles.buttonView2]}
                            >
                                <Text style={styles.btnText}>Save</Text>
                            </TouchableOpacity>
                        }
                        </View>
                    </ScrollView>
                }
            </View>

        </View>
    )

}
export default BookAppointment
