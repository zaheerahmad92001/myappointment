import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appMarginHorizontal, small,} from "../../Constants/appFontSize";
import {black, darkGreen, green, lightGreen, white } from "../../Constants/Colors";

const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginTop:hp(2),
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },
    imgView:{
        width:50,
        height:50,
        overflow:'hidden',
        backgroundColor:lightGreen,
        borderRadius:5,
    },
    imageStyle:{
        width:undefined,
        height:undefined,
        flex:1,
    },
    docView:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:"center",
        marginTop:hp(1),
    },
    nameView:{
        alignSelf:"flex-start",
        marginTop:2,
        marginLeft:10,
        flex:1,
    },
    nameText:{
        color:black,
        fontSize:small,
        fontWeight:'500',
    },
    ticketNum:{
     color:green,
     fontWeight:'500',
     fontSize:small,
     textAlign:'right',
    },

modalDropdown:{
    width:"100%",
    justifyContent:"center",
    borderColor:lightGreen,
   },
   dropdownTextStyle:{
    color:black,
    fontSize:small, 
    fontWeight:'500',
    marginLeft:10,
  },
  dropdownTextHighlightStyle:{
    color:"black",
    borderColor:white,
    borderWidth:0,
   },
   textStyle:{
       color:black, 
       fontSize:small,
       fontWeight:"500",
        marginLeft:10,
    },
    dropdownStyle:{
          fontSize:small,
          fontWeight:"500"
        },
    ddHeader:{
    marginLeft:10,
    marginBottom:5,
    color:green,
    fontSize:small,
    fontWeight:'400',
    
    }, 
    ddOuterView:{
        borderWidth:1,
        borderColor:lightGreen,
        paddingVertical:10,
        marginTop:hp(2),
        
    },
    dropdown_row:{
      paddingVertical:10,
      paddingLeft:10,
    },
    dropdown_row_text:{
    fontWeight:'500',
    fontSize:small,
    color:black
    },
    montText:{
        color:green,
        fontSize:small,
        fontWeight:"500",
        // marginTop:hp(3)
    },
    timeText:{
        color:green,
        fontSize:small,
        fontWeight:"500",
        marginBottom:hp(3)
    },
    dateViewSelect:{
        height:60,
        paddingHorizontal:5,
        marginTop:hp(2),
        backgroundColor:darkGreen,
        borderRadius:10,
        justifyContent:"center",
        alignItems:'center',
        marginRight:15,
    },
    dateView:{
        height:60,
        paddingHorizontal:5,
        marginTop:hp(2),
        backgroundColor:white,
        borderRadius:10,
        justifyContent:"center",
        alignItems:'center',
        marginRight:15,
    },
    dateTextSelect:{
        fontWeight:'500',
        fontSize:small,
        color:white
    },
    dateText:{
        fontWeight:'500',
        fontSize:small,
        color:darkGreen
    },
    timOuterView:{
        marginTop:hp(3)
    },
    timeContainer:{
        backgroundColor:white,
        paddingHorizontal:15,
        paddingVertical:10,
        marginRight:10,
        borderRadius:5,
        marginBottom:hp(1)
    },
    timeContainerSelected:{
        backgroundColor:lightGreen,
        paddingHorizontal:15,
        paddingVertical:10,
        marginRight:10,
        borderRadius:5,
        marginBottom:hp(1)
    },

    tiemText:{
        color:darkGreen,
        fontSize:small,
        fontWeight:'500',
    },
    tiemTextSelected:{
        color:white,
        fontSize:small,
        fontWeight:'500',
    },
    buttonView:{
        justifyContent:"center",
        alignItems:'center',
        width:wp(80),
        paddingVertical:10,
        backgroundColor:darkGreen,
        alignSelf:'center',
        marginBottom:20,
    },
    buttonView2:{
        justifyContent:"center",
        alignItems:'center',
        width:wp(80),
        paddingVertical:10,
        backgroundColor:darkGreen,
        alignSelf:'center',
        marginBottom:20,
        marginTop:hp(10)
    },

    btnText:{
        color:white,
        fontSize:14,
        fontWeight:'600'
    }
    
})

export default styles