import React, { useEffect, } from 'react'
import { View, Text, Image, StyleSheet } from 'react-native'
import * as Animatable from 'react-native-animatable';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { appIcon } from '../../Constants/Images'
import { white } from '../../Constants/Colors';

function Splash({ navigation }) {

    useEffect(() => {
        runApp()
    })

    async function runApp() {
        setTimeout(() => {
            // navigation.navigate('HomeStack')
            navigation.navigate('AuthStack')
        }, 2000)
    }

    return (
        <View style={styles.wraper}>
            <Animatable.Image
                animation="zoomInUp"
                easing="ease-in"
                duration={2000}
                delay={500}
                resizeMode={'contain'}
                source={appIcon}
                style={styles.appIconStyle}
            />
        </View>
    )
}
export default Splash

const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        justifyContent: "center",
        alignItems: 'center',
        backgroundColor: white
    },
    appIconStyle: {
        width: '100%',
        height: 250,
        resizeMode: 'contain'
    },
})