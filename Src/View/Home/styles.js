import React from "react";
import { View, StyleSheet, Platform } from "react-native";
import { RFValue } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appMarginHorizontal, headerHeigh, small, statusBar } from "../../Constants/appFontSize";
import { black, darkBlue, green, lightBlack, lightGreen, white } from "../../Constants/Colors";


const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    headerView: {
        marginTop: headerHeigh,
        marginHorizontal: 10,
        // justifyContent:'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
    },
    headerImg: {
        width: wp(40),
        height: 50,
    },

    headerLeft: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    placeImg: {
        width: 20,
        height: 50,
    },
    placeText: {
        fontWeight: '600',
        fontSize: small,
        color: black,
        marginLeft: 5,
    },
    container: {
        // marginTop: statusBar,
        marginTop: hp(2),
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },
    iconStyle: {
        fontSize: 15,
        marginLeft: 10,
        marginRight: 10,
    },
    searchView: {
        borderRadius: 20,
        borderWidth: 1,
        borderColor: 'grey',
        paddingVertical: Platform.OS=='ios'? 10:0,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems:"center",
        paddingHorizontal: 10,
    },
    searchIcon: {
        fontSize: 20,
        color: lightBlack,
    },
    catView: {
        marginTop: hp(2),
    },
    standerdText: {
        color: black,
        fontWeight: '500',
        fontSize: small,
    },
    smallText:{
        color: green,
        fontWeight: '500',
        fontSize:RFValue(12),
    },
    catBG: {
        backgroundColor: darkBlue,
        width: 100,
        height: 100,
        paddingVertical: 10,
        marginRight: 20,
        borderRadius: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    carRender: {
        marginTop: hp(3),
    },
    catImg: {
        width: 50,
        height: 50,
        resizeMode: 'contain'
    },
    catName: {
        textAlign: 'center',
        marginRight: 20,
        marginTop: 10,
    },
    appointmentView: {
        marginTop: hp(3),
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    rowSpBw: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'space-between',
    },
    appointmentsView: {
        flexDirection: 'row',
        alignItems: 'center',
        marginTop: hp(2)
    },
    circle: {
        width: 25,
        height: 25,
        borderRadius: 25 / 2,
        backgroundColor: lightGreen,
        // marginLeft: 10,
        alignItems: 'center',
    },
    ligtGreenBG: {
        backgroundColor: lightGreen,
        paddingVertical: hp(2),
        justifyContent:'center',
        alignItems:'center',
        marginBottom:10,
        marginRight:20
    },
    addStyle:{ 
        width: wp(45), 
        borderBottomWidth:0.6,
        borderBottomColor:lightBlack,
        justifyContent:'space-between',
    },
    moreView:{
        backgroundColor:lightGreen,
        paddingVertical:5,
        paddingHorizontal:10,
    },
    imgStyle:{
        width:40,
        height:40,

        // width:70,
        // height:70,
        
        marginLeft:10,
        resizeMode:'contain',
        overflow:'hidden',
    },
    recentItemView:{
        flexDirection:'row',
        marginTop: hp(2),
        marginLeft:10,
    },
    recentItemContent:{
        marginLeft:5,
    }
    
})

export default styles