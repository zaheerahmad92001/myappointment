import React, { useEffect, useReducer } from "react";
import { View, Text, Image, TextInput, FlatList, Pressable } from "react-native";
import { Divider, Icon } from "react-native-elements";

import { headerIcon, place, shops, facilityVactor,individaul } from '../../Constants/Images'
import { black, darkBlue, green, lightBlack, orange, parror ,carrot } from '../../Constants/Colors'
import styles from "./styles";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scrollview";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { getServices } from "../../Utility/Api";


let data = [
    { id: 1 , tag:'Shops' , img:shops, color:darkBlue},
    { id: 2 ,tag:'Facilities',img:facilityVactor , color:orange},
    { id: 3 , tag:'Individual', img:individaul, color:carrot },
]

function Home({ navigation }) {
    useEffect(() => {
    //  fetchServices()
    }, [])

    const [state, updateState] = useReducer((
        (state, newState) => ({ ...state, ...newState })
    ), {
        searhText: '',
    })
    const { searhText } = state


    function SearchBar() {
        return (
            <View style={styles.searchView}>
                <TextInput
                    placeholder="Search"
                    placeholderTextColor={lightBlack}
                    onChangeText={(text) => handleSearch(text)}
                    value={searhText}
                />
                <Icon
                    name={'search1'}
                    type={'antdesign'}
                    iconStyle={styles.searchIcon}
                />
            </View>
        )
    }

    function handleSearch(value) {
        updateState({ searhText: value })
    }

    function Categories() {
        return (
            <View style={styles.catView}>
                <Text style={styles.standerdText}>What are you looking for?</Text>
                <FlatList
                    data={data}
                    horizontal={true}
                    keyExtractor={(item) => item.id}
                    renderItem={renderCategories}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        )
    }

    const renderCategories = ({ item, index }) => {
        return (
            <Pressable 
            onPress={()=>{navigation.navigate('Listing',{
                params:item
            })}}
            style={styles.carRender}>
                <View style={[styles.catBG,{backgroundColor:item.color}]}>
                    <Image
                        source={item.img}
                        // source={individaul}
                        style={styles.catImg}
                    />
                </View>
                <Text style={styles.catName}>{item.tag}</Text>
            </Pressable>
        )
    }

    function UpcommingAppointment() {
        return (
            <View style={styles.appointmentView}>
                <Text style={styles.standerdText}>Upcoming Appointments</Text>
                <View style={[styles.row, { marginTop: 20 ,}]}>
                    <View style={[styles.row,styles.addStyle]}>
                        <Text style={[styles.standerdText,{marginBottom:5}]}>{'Alhabeeb Hospital'}</Text>
                        {/* <View style={[styles.circle,{marginBottom:5}]}><Icon type={'ionicon'} name={'chevron-forward'} iconStyle={{ color:green,fontSize:20}}/></View>   */}
                    </View>
                    
                    <View style={{ width: wp(45), marginLeft:20, borderBottomWidth:1, borderBottomColor:lightBlack }}>
                        <Text style={[styles.standerdText,{marginBottom:5}]}>{'Okku'}</Text>
                    </View>
                </View>

                <FlatList
                    data={data}
                    horizontal={true}
                    keyExtractor={(item) => item.id}
                    renderItem={renderUpcommingAppointment}
                    showsHorizontalScrollIndicator={false}
                />
               
            </View>
        )
    }

    const renderUpcommingAppointment = ({ item, index }) => {
        return (
            <Pressable 
            style={styles.appointmentsView}>
              <View style={{width:wp(50),}}>
                  <View style={styles.ligtGreenBG}><Text style={[styles.standerdText,{color:black}]}>{'Apppoint with zaher'}</Text></View>
                  <View style={styles.ligtGreenBG}><Text style={[styles.standerdText,{color:parror}]}>{'12:pm'}</Text></View>
              </View>
            </Pressable>
        )
    }

    function RecentlyAdded(){
        return(
            <View style={styles.appointmentView}>
                <View style={styles.rowSpBw}>
              <Text style={styles.standerdText}>Recently Added</Text>
              {/* <Pressable 
               onPress={()=>{}}
               style={[styles.row,styles.moreView]}>
                  <Text style={styles.smallText}>{'More'}</Text>
                  <Icon type={'ionicon'} name={'chevron-forward'} iconStyle={{ color:green,fontSize:20}}/>
              </Pressable> */}
              </View>
              <FlatList
                    data={data}
                    horizontal={true}
                    keyExtractor={(item) => item.id}
                    renderItem={renderRecentlyAdded}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
        )
    }

    const renderRecentlyAdded=({item , index})=>{
        return(
            <View style={{marginBottom:20}}>
                <View style={styles.recentItemView}>
                <View style={styles.imgStyle}>
                  <Image
                    source={shops}
                    style={{width:undefined, height:undefined, flex:1}}
                  />
                </View>
                <View style={styles.recentItemContent}>
                    <Text style={[styles.standerdText]}>{'Colliflower'}</Text>
                    <Text style={[styles.smallText]}>{'Colliflower'}</Text>
                </View>
                </View>
            </View>
        )
    }

    return (
        <View style={styles.wraper}>
            <View style={styles.headerView}>
                <Image
                    source={headerIcon}
                    style={styles.headerImg}
                    resizeMode="contain"
                />
                {/* <View style={styles.headerLeft}>
                    <Image
                        source={place}
                        style={styles.placeImg}
                        resizeMode="contain"
                    />
                    <Text style={styles.placeText}>Ridhya</Text>
                    <Icon
                        type={'antdesign'}
                        name={'down'}
                        iconStyle={styles.iconStyle}
                    />
                </View> */}
            </View>
            <View style={styles.container}>
                <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                >
                    {/* {SearchBar()} */}
                    {Categories()}
                    {UpcommingAppointment()}
                    {RecentlyAdded()}
                </KeyboardAwareScrollView>
            </View>
        </View>
    )
}

export default Home;