import React, { useEffect, useRef, useReducer } from 'react'
import { View, Text, Image, StyleSheet, Platform, Pressable } from 'react-native'
import IntlPhoneInput from 'react-native-intl-phone-input';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scrollview';
import AppButton from '../../Components/AppButton';

import { appMarginHorizontal, statusBar } from '../../Constants/appFontSize'
import { black, darkgrey, lightBlack, white } from '../../Constants/Colors'
import { appIcon } from '../../Constants/Images'
import ContactInput from '../../Components/ContactInput';

import { styles } from './styles';

function Login({ navigation }) {
    const phoneInput = useRef()

    useEffect(() => {

    }, [])

    const [state, updateState] = useReducer((
        (state, newState) => ({ ...state, ...newState })
    ), {
        cca2: 'PK',
        callingCode: '92',
        phoneNum: '',
        isVerified: false
    })

    const { cca2, callingCode, phoneNum, isVerified } = state

    // const handlePhoneNum = (value) => {
    //     updateState({
    //         phoneNum: value.phoneNumber,
    //         isVerified: value.isVerified
    //     })
    //     //   console.log('here is value', value.unmaskedPhoneNumber);
    // }

    const _navigate = () => {
        if (phoneNum.length >5) {
            navigation.navigate('HomeStack')
        }else{
            alert('Please enter Valid Phone Number')
        }
    }

    return (
        <View style={styles.wraper}>
            <View style={styles.container}>
                <KeyboardAwareScrollView>

                    <Image
                        animation="zoomInUp"
                        easing="ease-in"
                        duration={2000}
                        delay={500}
                        resizeMode={'contain'}
                        source={appIcon}
                        style={styles.appIconStyle}
                    />
                    <View style={styles.content}>
                        <ContactInput
                            cca2={cca2}
                            callingCode={callingCode}
                            onChangeText={value => updateState({ phoneNum: value })}
                            contact={phoneNum}
                            select={(country) => {
                            updateState({ cca2: country.cca2, callingCode: country.callingCode })
                            }}
                        />
                        {/* <IntlPhoneInput
                        ref={phoneInput}
                        onChangeText={(c) => handlePhoneNum(c)}
                        flagStyle={{ fontSize: Platform.OS == 'android' ? 15 : 25, }}
                        phoneInputStyle={{backgroundColor:white,color:black
                            // borderColor:lightBlack, borderWidth:1,paddingVertical:5
                        }}
                        // customModal={this.renderCustomModal}
                        defaultCountry="PK"
                        lang="EN"
                    /> */}
                        <AppButton
                            title={'Confirm'}
                            onPress={() => _navigate()}
                            buttonContainer={[styles.buttonContainer]}
                        />
                        {/* <Pressable style={styles.signInBtn}>
                        <Text style={styles.btnText}>Already have an account? Sign in!</Text>
                    </Pressable> */}
                    </View>
                </KeyboardAwareScrollView>
            </View>
        </View>
    )
}
export default Login;

