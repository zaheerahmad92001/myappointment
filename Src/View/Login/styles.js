import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { appMarginHorizontal, small, statusBar } from "../../Constants/appFontSize";
import { green, white } from "../../Constants/Colors";

export const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        marginTop: statusBar,
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },
    appIconStyle: {
        width: '100%',
        height: 250,
        resizeMode: 'contain'
    },
    content: {
        marginTop: hp(10)
    },
    buttonContainer:{
        alignSelf:'center',
        marginTop:hp(5)
    },
    signInBtn:{
        marginTop:hp(5),
        justifyContent:'center',
        alignItems:'center',
    },
    btnText:{
        color:green,
        fontSize:small,
        fontWeight:'600',
    }
})