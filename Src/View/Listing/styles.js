import { StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { appMarginHorizontal, mediumSize,} from "../../Constants/appFontSize";
import {darkGreen, white } from "../../Constants/Colors";

const styles = StyleSheet.create({
    wraper: {
        flex: 1,
        backgroundColor: white
    },
    container: {
        // marginTop: statusBar,
        marginTop:hp(2),
        marginHorizontal: appMarginHorizontal,
        flex: 1,
    },
    flatList:{
        paddingTop:hp(4)
    },
    middlePage:{ 
        flex: 1, 
        justifyContent: 'center',
         alignItems: 'center' 
        },
        textStyle:{
            fontSize:mediumSize,
            fontWeight:'500',
            color:darkGreen
        }
    
    
})

export default styles