import React, { useEffect, useReducer } from "react";
import { View, FlatList, Alert, ActivityIndicator, Text } from "react-native";

import styles from "./styles";
import AppHeader from "../../Components/AppHeader";
import { Shop_Tabs, Fac_Tabs } from '../../Constants/ConstantValues'
import ShopsTabs from "../../Components/ShopsTab";
import ItemCart from "../../Components/ItemCard";
import { getServices } from "../../Utility/Api";
import { darkGreen } from "../../Constants/Colors";
import symbolicateStackTrace from "react-native/Libraries/Core/Devtools/symbolicateStackTrace";


function Listing({ navigation, route }) {

    useEffect(() => {
        let tag = route.params.params.tag
        fetchServices(tag)

    }, [])

    const [state, updateState] = useReducer((
        (state, newState) => ({ ...state, ...newState })
    ), {
        searchText: '',
        servicesList: '',
        servicesListCopy:undefined,

        loading: false,
        header: route.params.params.tag , 
        tabsList:Shop_Tabs,
        searchActive:false,

    })
    const {
        searchText,
        servicesList,
        header,
        loading,
        tabsList,
        searchActive,
        servicesListCopy
    } = state



    async function fetchServices(tag) {
        updateState({ loading: true })
        await getServices(tag).then((res) => {
            const { data } = res
            if (data?.api_status) {

                updateState({
                    servicesList: data?.services,
                    servicesListCopy:data?.services,
                    loading: false
                })
            } else {
                Alert.alert('Error Occure')
                updateState({ loading: false })
            }
        }).catch((err) => {
            updateState({ loading: false })
            Alert.alert('Error Occure')
            console.log('error occure', err);
        })
    }


    function handleShopList(tab , index) {
        const list = tabsList.slice()
        let temp = []
        list.map((item, i)=>{
            if(i==index){
                item.selected = true
                temp[i] = item
            }else{
                item.selected = false
                temp[i] = item
            }
            updateState({tabsList:temp})
            fetchServices(tab.tag)
        })
}

function handleLeftPress(){
    if(searchActive){
      updateState({searchActive:false})
    }else{
        navigation.pop()
    }
}

    function renderShopItem({ item }) {
        return (
            <ItemCart
                item={item}
                onPress={()=>navigation.navigate('BookAppointment',{
                    params:{item}
                })}
            />
        )
    }

    const handleSearch=(text)=>{
        let tempList = JSON.parse(JSON.stringify(servicesList))
       
         const filterData = tempList.filter((obj)=>{
            const regex = new RegExp([text.trim()], 'i')
              if(obj.title.search(regex) >= 0){
                return obj
            }
         })
         
         if(text.trim().length > 0){
         updateState({servicesList:filterData, searchText:text})
         }else{
         updateState({servicesList:servicesListCopy , searchText:text})
         }
      }
      


    return (
        <View style={styles.wraper}>
            <AppHeader
                title={header}
                leftIconName={searchActive ?'close':'arrow-back-ios'}
                leftIconType={'material'}
                leftPress={() => handleLeftPress()}

                rightIconName={'search1'}
                rightIconType={'antdesign'}
                rightPress={() =>updateState({searchActive:true}) }
                inputSearchText={(text)=> handleSearch(text)}
                searchText={searchText}
                search={searchActive}
            />
            <View style={styles.container}>
                <ShopsTabs
                    Tabs={tabsList}
                    onPress={handleShopList}
                />
                {loading ?
                    <View style={styles.middlePage}>
                        <ActivityIndicator
                            color={darkGreen}
                            size={'small'}
                        />
                    </View>
                    :servicesList?.length > 0?
                    <FlatList
                        data={servicesList}
                        renderItem={renderShopItem}
                        keyExtractor={item => item.id}
                        numColumns={2}
                        columnWrapperStyle={{justifyContent:'space-between'}}
                        showsVerticalScrollIndicator={false}
                        style={styles.flatList}

                    /> :
                    <View style={styles.middlePage}>
                     <Text style={styles.textStyle}>{'Record Not Found'}</Text>
                    </View>
                }

            </View>

        </View>
    )

}
export default Listing
