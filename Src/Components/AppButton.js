import React from "react";
import { View, Text, StyleSheet, Pressable } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { small } from "../Constants/appFontSize";
import { darkgrey, lightGreen, orange, white } from "../Constants/Colors";

const AppButton = (props) => {
    const {title , onPress} = props
    return (
        <Pressable 
        onPress={onPress}
         style={[styles.wraper,props.buttonContainer]}>
            <Text style={[styles.TextStyle,props.textStyle]}>{title}</Text>
        </Pressable>
    )
}
export default AppButton;

const styles = StyleSheet.create({
    wraper: {
        backgroundColor:orange,
        width: wp(85),
        paddingVertical: hp(1.8),
        borderRadius:5,
        justifyContent:'center',
        alignItems:'center',
    },
    TextStyle:{
        color:white,
        fontSize:small,
    }
})
