import React from "react";
import { View, Text, Image, StyleSheet, Pressable } from "react-native";
import { Icon } from "react-native-elements";
import { RFValue } from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { mediumSize, small } from "../Constants/appFontSize";
import { black, darkGreen, green, lightWhite, white } from "../Constants/Colors";
import { Rectangle } from '../Constants/Images'
import { URL } from "../Utility/Api";

const ItemCart = (props) => {
    const {item ,onPress} = props

    return (
        <Pressable
         onPress={onPress}
         style={styles.wraper}>
            <View style={styles.rowSpaceBtw}>
                <View style={styles.halfView}>
                    <View style={styles.imgView}>
                        <Image
                            source={{uri:`${URL}${item.attachments[0]?.url}`}}
                            style={styles.img}
                        />
                    </View>
                </View>

                <View style={styles.halfView}>
                    <View style={styles.ratingView}>
                        <View style={styles.row}>
                            <Icon name={'star'} type={'entypo'} iconStyle={styles.iconStyle} />
                            <Text style={styles.standerdText}>{item?.ratings}</Text>
                        </View>
                    </View>
                    <Text style={styles.app_no}>MP# 12454</Text>
                </View>
            </View>
            <View style={styles.content}>
                <Text style={styles.mediumText}>{item?.title}</Text>
                <Text style={[styles.standerdText,styles.smallText]}>{item?.tag_list[0]}</Text>
            </View>
        </Pressable>
    )
}
export default ItemCart

const styles = StyleSheet.create({
    wraper: {
        width: wp(40),
        paddingBottom:10,
        borderRadius: 10,
        borderColor:'#D7D7D7',
        borderWidth: 1,
        height: 'auto',
        // marginRight:wp(3),
        marginBottom:hp(4),
    },
    ratingView: {
        paddingVertical: 5,
        backgroundColor: darkGreen,
        borderTopRightRadius: 10,
        borderBottomLeftRadius: 10,
        width: wp(15),
        alignSelf: 'flex-end',
        justifyContent: 'center',
    },
    row: {
        flexDirection: 'row',
        alignItems:'center',
        // justifyContent:'center',
    },
    iconStyle: {
        color: white,
        fontSize: 20,
    },
    standerdText: {
        fontSize: small,
        color: white,
    },
    imgView: {
        width: 40,
        height: 40,
        overflow: 'hidden',
    },
    img: {
        flex: 1,
        width: undefined,
        height: undefined,
    },
    rowSpaceBtw: {
        flexDirection: 'row',
        // justifyContent:'space-between',
    },
    halfView: {
        width: wp(40 / 2)
    },
    app_no: {
        fontSize: RFValue(12),
        fontWeight: '500',
        color: darkGreen,
        marginTop: 10,
    },
    content: {
        marginHorizontal:5,
        marginTop:hp(2)
    },
    mediumText:{
        fontWeight:'600',
        fontSize:mediumSize,
        color:black,
    },
    smallText:{
        color:darkGreen,
        marginTop:hp(1),
    }
})
