import React from "react";
import { View ,Text, StyleSheet, FlatList, Pressable } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { small } from "../Constants/appFontSize";
import { darkGreen, green } from "../Constants/Colors";

const circleDimen = 6

const ShopsTabs =(props)=>{

    const {Tabs , onPress} = props
   
    const renderShopTabsList=({item, index})=>{
        return(
            <Pressable 
            onPress={()=>onPress(item, index)}
            style={styles.container}>
                <Text style={item.selected?[styles.nameTextSelected] : [styles.nameText]} >{item.name}</Text>
                {item.selected &&
                <View style={styles.circle}></View>
                }
            </Pressable>
        )
    }


    return(
     <View>
         <FlatList
         data={Tabs}
         renderItem={renderShopTabsList}
         keyExtractor={item=>item.id}
         horizontal={true}
         showsHorizontalScrollIndicator={false}
         />
     </View>
    )
}
export default ShopsTabs;

const styles = StyleSheet.create({
    wraper:{
      flexDirection:'row',
      alignItems:'center',

    },
    nameText:{
        color:darkGreen,
        fontSize:small,
        fontWeight:'500'
    },
    nameTextSelected:{
        color:darkGreen,
        fontSize:small,
        fontWeight:'bold'
    },
    container:{
        marginRight:hp(2)
    },
    circle:{
        width:circleDimen,
        height:circleDimen,
        borderRadius:circleDimen/2,
        backgroundColor:green,
        alignSelf:'center',
    }
})