import React from 'react'
import { View, Text, StyleSheet, TextInput, Platform } from 'react-native'
import { Icon } from 'react-native-elements';
import { widthPercentageToDP as wp } from 'react-native-responsive-screen';
import * as Animatable from 'react-native-animatable';

import { headerHeigh, mediumSize, small } from '../Constants/appFontSize';
import { black, darkGreen, white } from '../Constants/Colors';


const AppHeader = (props) => {
    const {
        title,
        leftIconName,
        leftIconType,
        leftPress,
        rightIconName,
        rightIconType,
        rightPress,
        dropdown,
        placeholder,
        handleSearch,
        inputSearchText,
        searchText,
        search
    } = props
    return (
        <View style={styles.wraper}>

            <View style={styles.left}>

                {leftIconName &&
                    <Icon
                        onPress={leftPress}
                        name={leftIconName}
                        type={leftIconType}
                        tvParallaxProperties={undefined}
                        iconStyle={styles.iconStyle}
                    />
                }
            </View>


            <View style={search ? [styles.Searchbody] : [styles.body]}>
                {search ?
                    <Animatable.View 
                    animation={'fadeInRight'}
                    duration={1500}
                    style={styles.searchBar}>
                        <TextInput
                            placeholder='type here'
                            placeholderTextColor={darkGreen}
                            onChangeText={inputSearchText}
                            value={searchText}
                            style={styles.searchBox}
                        />
                        <Icon
                            onPress={handleSearch}
                            name={rightIconName}
                            type={rightIconType}
                            tvParallaxProperties={undefined}
                            iconStyle={[styles.iconStyle, { left: 10, color: black }]}
                        />
                    </Animatable.View> :
                    <Text style={styles.centerText}>{title}</Text>
                }
            </View>
            {search ? null :
                <View style={styles.right}>
                    {rightIconName &&
                        <View style={styles.rightView}>
                            <Icon
                                onPress={rightPress}
                                name={rightIconName}
                                type={rightIconType}
                                tvParallaxProperties={undefined}
                                iconStyle={[styles.iconStyle, { left: 10, color: black }]}
                            />
                        </View>
                    }
                </View>
            }

        </View>
    )
}
export default AppHeader

const styles = StyleSheet.create({
    wraper: {
        marginTop: headerHeigh,
        marginHorizontal: 10,
        flexDirection: 'row',
        alignItems: 'center',
    },
    left: {
        width: wp(15),
    },

    iconStyle: {
        fontSize: 20,
        color: black,
        width: wp(10),
    },
    body: {
        width: wp(65)
    },
    Searchbody: {
        width: wp(75)
    },
    centerText: {
        textAlign: 'center',
        fontSize: mediumSize,
        paddingVertical: Platform.OS == 'android' ? 5 : 10,

    },
    right: {
        width: wp(15),
    },
    rightView: {
        backgroundColor: white,
        width: 30,
        height: 30,
        borderRadius: 30 / 2,
        justifyContent: 'center',
        alignSelf: 'center',
    },
    row: {
        flexDirection: "row",
        alignItems: "center",
        backgroundColor: white,
        shadowColor: 'red',
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.3,
        elevation: 2,
        width: wp(24),
        marginLeft: 10,

    },
    cname: {
        marginLeft: 5,
        fontSize: small,
        color: black
    },
    searchBar: {
        backgroundColor: white,
        paddingVertical: Platform.OS == 'android' ? 5 : 10,
        borderRadius: 5,
        shadowColor: '#000',
        shadowOffset: { height: 2, width: 2 },
        shadowOpacity: 0.2,
        shadowRadius: 2,
        elevation: 1,
        flexDirection: 'row',
    },
    searchBox: {
        flex: 1,
        marginLeft: 5,
    }

})