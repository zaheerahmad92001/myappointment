export const Shop_Tabs =[
    {id:1 , name:'All' , tag:'Individual', selected:false},
    {id:2 , name:'Restaurants' , tag:'Restaurants', selected:false},
    {id:3 , name:'Barbers' , tag:'Barbers', selected:false},
    {id:4 , name:'Beauty Salons' , tag:'Beauty Salons' , selected:false},
    {id:5 , name:'Cafe' , tag:'Cafe', selected:false},
]

export const Fac_Tabs =[
    {id:1 , name:'All' , screen:'All'},
    {id:2 , name:'Hospitals' , screen:'Hospitals'},
    {id:3 , name:'Companies' , screen:'Companies'},
    {id:4 , name:'Ministries' , screen:'Ministries'},
]